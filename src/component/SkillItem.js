import React, { Component, PropTypes } from 'react';
import {
    Image,
    StyleSheet,
    View,
    Text
} from 'react-native';
import TT_COLORS from '../constants/colors';

const emoticon = [
  require('./../../res/img/emoticon-bad.png'),
  require('./../../res/img/emoticon-ready.png'),
  require('./../../res/img/emoticon-good.png'),
  require('./../../res/img/emoticon-love.png')
];
const starOwned = require('./../../res/img/star_owned.png');
const starNotOwned = require('./../../res/img/star_not_owned.png');

export default class SkillItem extends Component {
  stars = (skill) => {
    let a;
    if (skill.skill.name === 'English') { a = [1, 2, 3, 4, 5]; } else a = [1, 2, 3, 4];
    return (
      a.map((row, i) => {
        const image = row <= skill.level ? starOwned : starNotOwned;
        return <Image key={i} source={image} />;
      })
    );
  };

  render() {
    return (
      <View style={styles.row}>
        <View style={styles.info}>
          <Text>{this.props.skill.skill.name}</Text>
          <Image
            style={styles.emoticon}
            source={emoticon[this.props.skill.attitude - 1]}
          />
        </View>
        <View style={styles.ratingContainer}>
          {this.stars(this.props.skill)}
        </View>
      </View>
    );
  }
}

SkillItem.propTypes = {
  skill: PropTypes.object
};

const styles = StyleSheet.create({
  row: {
    margin: 6,
    width: 112,
    height: 100,
    backgroundColor: TT_COLORS.TT_LIGHT_GRAY_BG,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#CCC'
  },
  info: {
    flex: 0.9,
    justifyContent: 'center',
    alignItems: 'center'
  },
  ratingContainer: {
    flex: 0.1,
    backgroundColor: TT_COLORS.TT_RATING_GRAY_BG,
    width: 110,
    height: 22,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  emoticon: {
    width: 33,
    height: 33,
    margin: 2
  }
});

import React, { Component, PropTypes } from 'react';
import {
    ListView,
    Image,
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Dimensions
} from 'react-native';

import TT_COLORS from '../constants/colors';

const dataSource = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2
});

const FiltersBackgroundColor = TT_COLORS.TT_LIGHT_GRAY_BG;
const BorderColor = TT_COLORS.TT_BORDER_GRAY;
const BorderWidth = 1;
const FilterInputBorderRadius = 5;
const FilterInputFontSize = 19;
const FilterInputHeight = 40;
const SkillFilterHorizontalOffset = 8;
const SkillInputPadding = 8;
const MinSkillInputWidth = 150;
const maxSkillInputWidth = () => {
  return Dimensions.get('window').width - SkillFilterHorizontalOffset - SkillInputPadding - BorderWidth;
};

export default class Filters extends Component {
  state = {
    selected: new Set(),
    skillInputText: '',
    skillInputWidth: maxSkillInputWidth(),
    skillsDS: dataSource.cloneWithRows([])
  };

  render() {
    return (
      <View>
        {this.nameSearcher()}
        {this.skillsFilter()}
        {this.seperator()}
      </View>
    );
  }

  nameSearcher = () =>
    <View style={styles.filterByNameView}>
      <TextInput
        style={styles.nameInput}
        placeholder='Filter by name'
        multiline={false}
        underlineColorAndroid={'transparent'}
        disableFullscreenUI={true}
        onChangeText={text => this.props.onChangeText(text)}
        autoFocus={true}
      />
    </View>;


  skillsFilter = () => {
    if (this.props.allFilters) {
      return (
        <View style={{ paddingHorizontal: SkillFilterHorizontalOffset, marginBottom: 7 }}>
          <View style={styles.filterBySkillsView}>
            <View style={styles.selectedSkillsContainer}>
              {this.selectedSkills(this.state.selected)}
              <TextInput
                style={skillInputStyle(this.state.skillInputWidth)}
                placeholder='Filter by skills'
                multiline={false}
                underlineColorAndroid={'transparent'}
                disableFullscreenUI={true}
                value={this.state.skillInputText}
                onChangeText={this.onSkillInputTextChanged}
              />
            </View>
          </View>
          {this.suggestedSkillsView()}
        </View>

      );
    }

    return null;
  };

  suggestedSkillsView = () => {
    if (this.state.skillsDS.getRowCount() === 0) {
      return null;
    }
    return (
      <View style={styles.suggestedSkillsView}>
        <ListView
          dataSource={this.state.skillsDS}
          enableEmptySections={true}
          renderRow={this.renderSkillsRow}
          keyboardShouldPersistTaps={true}
        />
      </View>
    );
  };

  seperator = () => {
    return (
      <View style={{ flexDirection: 'row', height: 1, backgroundColor: BorderColor }} />
    );
  };

  selectedSkills = (skills) => {
    if (skills.size === 0) {
      const measuredSkillInputWidth = maxSkillInputWidth();
      if (measuredSkillInputWidth !== this.state.skillInputWidth) {
        this.setState({ skillInputWidth: measuredSkillInputWidth });
      }
      return null;
    }
    const array = Array.from(skills);
    return (
            array.map((skill, i, arr) => {
              const desiredIndex = arr.length - 1;
              const onLayout = (i === desiredIndex) ? this.onLastSelectedSkillViewLayout : null;
              return (
                <TouchableOpacity
                  underlayColor={TT_COLORS.TRANSPARENT}
                  onPress={() => this.onPressSkillCloseButton(skill)}
                  style={styles.skillContainer}
                  key={i}
                  onLayout={(event) => { if (onLayout !== null) { onLayout(event); } }}
                >
                  <Text style={{ color: TT_COLORS.WHITE, fontWeight: 'bold' }}>{skill.name}</Text>
                  <Image
                    style={styles.closeImage}
                    source={require('./../../res/img/close-outline.png')}
                  />
                </TouchableOpacity>
              );
            })
    );
  };

  onLastSelectedSkillViewLayout = (event) => {
    const x = event.nativeEvent.layout.x;
    const width = event.nativeEvent.layout.width;
    const theMaxSkillInputWidth = maxSkillInputWidth();
    let measuredSkillInputWidth = theMaxSkillInputWidth - x - width;
    if (measuredSkillInputWidth < MinSkillInputWidth) {
      measuredSkillInputWidth = theMaxSkillInputWidth;
    }
    if (measuredSkillInputWidth !== this.state.skillInputWidth) {
      this.setState({ skillInputWidth: measuredSkillInputWidth });
    }
  };

  findSkills(query) {
    if (query === '') {
      return [];
    }
    const allSkills = this.props.skills;
    const regex = new RegExp(`${query.trim()}`, 'i');
    return allSkills.filter(skills => skills.name.search(regex) >= 0);
  }

  onPressSkillCloseButton = (skill) => {
    const selected = this.state.selected;
    selected.delete(skill);
    this.setState({ selected: selected });
    this.props.onSkillSelected(Array.from(selected));
  };

  onSkillInputTextChanged = (skillInputText) => {
    this.setState({ skillInputText });
    const data = this.findSkills(skillInputText);
    this.setState({ skillsDS: dataSource.cloneWithRows(data) });
  };

  renderSkillsRow = (rowData, sectionID, rowID) => {
    const style = this.state.selected.has(rowData) ? styles.rowItemSelected : styles.rowItem;
    return (
      <View>
        <TouchableOpacity
          underlayColor={TT_COLORS.TRANSPARENT}
          onPress={() => this._onSelectSkillsRow(rowData)}
        >
          <View style={{ flexDirection: 'row', backgroundColor: TT_COLORS.WHITE }}>
            <Text style={style}>{rowData.name}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  _onSelectSkillsRow(value) {
    const selected = this.state.selected;
    if (!selected.delete(value)) {
      selected.add(value);
    }
    this.setState({ selected: selected });

    this.props.onSkillSelected(Array.from(selected));
    this.setState({ skillInputText: '',
      skillsDS: dataSource.cloneWithRows([]) });
  }
}

Filters.propTypes = {
  allFilters: PropTypes.bool,
  onChangeText: PropTypes.func,
  onSkillSelected: PropTypes.func,
  skills: PropTypes.array
};

const skillInputStyle = (width) => {
  return ({
    justifyContent: 'center',
    height: FilterInputHeight,
    paddingHorizontal: 8,
    paddingTop: 0,
    paddingBottom: 0,
    fontSize: FilterInputFontSize,
    borderRadius: FilterInputBorderRadius,
    width: width
  });
};

const styles = StyleSheet.create({
  filterByNameView: {
    paddingHorizontal: 8,
    paddingVertical: 8,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'stretch',
    backgroundColor: FiltersBackgroundColor
  },
  filterBySkillsView: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    backgroundColor: FiltersBackgroundColor
  },
  selectedSkillsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: TT_COLORS.WHITE,
    borderRadius: FilterInputBorderRadius,
    borderWidth: BorderWidth,
    borderColor: BorderColor,
    marginBottom: 1
  },
  nameInput: {
    height: FilterInputHeight,
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingTop: 0,
    paddingBottom: 0,
    alignItems: 'center',
    fontSize: FilterInputFontSize,
    backgroundColor: TT_COLORS.WHITE,
    borderRadius: FilterInputBorderRadius,
    borderWidth: BorderWidth,
    borderColor: BorderColor
  },
  rowItem: {
    fontSize: 18,
    padding: 3
  },
  rowItemSelected: {
    fontSize: 18,
    padding: 3,
    color: TT_COLORS.TT_LIGHT_GRAY_BG
  },
  skillContainer: {
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: FilterInputBorderRadius,
    backgroundColor: TT_COLORS.TT_BLUE,
    marginLeft: 5,
    marginTop: 5,
    paddingLeft: 5,
    paddingRight: 3
  },
  suggestedSkillsView: {
    maxHeight: 210,
    borderWidth: BorderWidth,
    borderColor: BorderColor,
    backgroundColor: TT_COLORS.WHITE
  },
  closeImage: {
    width: 16,
    height: 16
  }
});

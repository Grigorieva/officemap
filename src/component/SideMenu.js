import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    Image,
    AsyncStorage,
    NetInfo,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { getEmployeeFromDB, getImageFromDB, clearData } from '../database/RealmQueries';
import { loadAllData } from '../network/loadAllData';
import { getEmployeeInfo } from '../network/loadEmployeeInfo';

import TT_COLORS from '../constants/colors';

const md5 = require('./../utils/js-md5');

export const SideMenuWidth = 200;

export default class SideMenu extends Component {
  state = {
    email: '',
    pressedElement: '',
    isFetching: null,
    thumbUri: './../../res/img/ic_account_circle_white_48dp.png'
  };

  componentDidMount() {
    AsyncStorage.getItem('uid', (err, id) => {
      NetInfo.isConnected.fetch().then((isConnected) => {
        if (isConnected) {
          this.loadNetwork(id);
        } else {
          this.getLocalData(id);
          this.setState({ isFetching: false });
        }
      });
    });
  }

  loadNetwork = (id) => {
    this.props.perform(getEmployeeInfo)(id).then((data) => {
      this.setState(data);
      const uri = `https://secure.gravatar.com/avatar/${md5(data.email)}.jpg?s=400&amp;r=g&amp;d=mm`;
      this.setState({ thumbUri: uri });
    });
  };

  getLocalData = (id) => {
    const data = getEmployeeFromDB(id);
    this.setState(data);
    const uri = getImageFromDB(id);
    this.setState({ thumbUri: uri });
  };

  renderLogoutButton = () => {
    return (
      <TouchableOpacity
        onPress={this.onLogoutPress.bind(this)}
        style={styles.logoutButton}
      >
        <Image
          style={{
            tintColor: TT_COLORS.WHITE,
            width: 20,
            height: 20,
            transform: [{ rotate: '180deg' }]
          }}
          source={require('./../../res/img/logout.png')}
        />
      </TouchableOpacity>
    );
  };

  render() {
    let fetchText;
    if (this.state.isFetching === null) { fetchText = 'Fetch data'; } else if (this.state.isFetching) { fetchText = 'loading...'; } else fetchText = '';
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={this.onUserContainerPress.bind(this)}
          style={styles.userContainer}
        >
          <Image
            style={styles.thumb}
            source={{
              uri: this.state.thumbUri
            }}
            defaultSource={require('./../../res/img/ic_account_circle_white_48dp.png')}
          />
          <Text style={styles.itemText}>{this.state.last_name} {this.state.first_name}</Text>
        </TouchableOpacity>
        {this.menuItem('Employees List', this.onEmployeesListPress.bind(this))}
        {this.menuItem(fetchText, this.onFetchDataPress.bind(this))}
        {this.renderLogoutButton()}
      </View>
    );
  }

  menuItem = (text, onPress) => {
    return (
      <TouchableOpacity
        style={styles.item}
        onPress={onPress}
      >
        <Text style={styles.itemText}>{text}</Text>
      </TouchableOpacity>
    );
  };

  onEmployeesListPress() {
    const drawer = this.context.drawer;
    drawer.close();
    Actions.employeesList();
  }

  onFetchDataPress() {
    if (!this.state.isFetching) {
      this.setState({ isFetching: true });
      loadAllData().then(() => this.setState({ isFetching: false }));
    }
  }

  onUserContainerPress() {
    const drawer = this.context.drawer;
    drawer.close();
    Actions.myProfile();
  }

  onLogoutPress() {
    fetch('https://planner.thumbtack.net/logout/').then(() => {
      AsyncStorage.removeItem('uid', () => {
        clearData();
        Actions.login();
      });
    });
  }
}

SideMenu.propTypes = {
  name: PropTypes.string,
  config: PropTypes.object,
  perform: PropTypes.func
};

SideMenu.contextTypes = {
  drawer: PropTypes.object
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: TT_COLORS.TT_SIDE_MENU_BG,
    width: SideMenuWidth
  },
  userContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: TT_COLORS.TT_SIDE_MENU_USER_BG,
    width: SideMenuWidth,
    height: 128
  },
  thumb: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  item: {
    width: SideMenuWidth,
    alignItems: 'flex-start',
    paddingLeft: 10,
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: TT_COLORS.TRANSPARENT
  },
  itemText: {
    color: TT_COLORS.WHITE,
    fontSize: 18
  },
  logoutButton: {
    position: 'absolute',
    left: 0,
    ...Platform.select({
      ios: {
        top: 20
      },
      android: {
        top: 0
      }
    }),
    paddingLeft: 5,
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 20,
    height: 40
  }
});

import React, { Component } from 'react';
import { Actions, DefaultRenderer } from 'react-native-router-flux';
import Drawer from 'react-native-drawer';

import SideMenu, { SideMenuWidth } from './SideMenu';

export default class NavigationDrawer extends Component {

  render() {
    const state = this.props.navigationState;
    const children = state.children;

    const { config, perform } = this.props;
    const apiProps = {
      config: config,
      perform: perform
    };

    return (
      <Drawer
        ref="navigation"
        open={state.open}
        onOpen={() => Actions.refresh({ key: state.key, open: true })}
        onClose={() => Actions.refresh({ key: state.key, open: false })}
        type='displace'
        content={<SideMenu {...apiProps} />}
        tapToClose={true}
        panOpenMask={0.25}
        panThreshold={0.25}
        openDrawerOffset={viewport => viewport.width - SideMenuWidth}
        negotiatePan={true}
        useInteractionManager={true}
        // tweenHandler={ratio => ({
        //   main: { opacity: Math.max(0.54, 1 - ratio) }
        // })}
      >
        <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
      </Drawer>
    );
  }
}

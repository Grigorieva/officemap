import {
  Image,
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  NetInfo
} from 'react-native';
import React, { Component, PropTypes } from 'react';
import { getImageFromDB } from '../database/RealmQueries';
import TT_COLORS from '../constants/colors';

const md5 = require('./../utils/js-md5');
const imagePlaceholder = require('./../../res/img/ic_account_circle_white_48dp.png');

export default class EmployeeItem extends Component {
  state = {
    thumbUri: './../../res/img/ic_account_circle_white_48dp.png'
  };

  componentDidMount() {
    this.loadThumbUri(this.props.employee);
  }

  componentWillReceiveProps(newProps) {
    this.loadThumbUri(newProps.employee);
  }

  loadThumbUri = (e) => {
    let uri;
    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected) {
        uri = `https://secure.gravatar.com/avatar/${md5(e.email)}.jpg?s=400&amp;r=g&amp;d=mm`;
      } else {
        uri = getImageFromDB((e.id));
      }
      this.setState({ thumbUri: uri });
    });
  };

  renderEmployeeDepartment() {
    const department = this.props.employee.employee_department;
    if (typeof department === 'undefined' || department === null) {
      return null;
    }

    return (
      <Text style={styles.textInfo}>
        {this.props.employee.employee_department.name}
      </Text>
    );
  }

  render() {
    return (
      <View style={{ backgroundColor: TT_COLORS.WHITE, overflow: 'hidden' }}>
        <TouchableHighlight
          underlayColor='transparent'
          onPress={() => this.props.onPress(this.props.employee)}
        >
          <View style={styles.rowContainer}>
            <Image
              style={styles.thumb}
              source={{ uri: this.state.thumbUri }}
              defaultSource={imagePlaceholder}
            />
            <View style={styles.textContainer}>
              <Text style={styles.name}>{this.props.employee.first_name} {this.props.employee.last_name}</Text>
              {this.renderEmployeeDepartment()}
              <Text style={styles.textInfo}>{this.props.employee.employee_title.name}</Text>
            </View>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

}

EmployeeItem.propTypes = {
  employee: PropTypes.object,
  onPress: PropTypes.func
};

const styles = StyleSheet.create({
  thumb: {
    width: 80,
    height: 80,
    marginRight: 10,
    borderRadius: 20
  },
  textContainer: {
    flex: 1
  },
  name: {
    fontSize: 22,
    color: TT_COLORS.BLACK
  },
  textInfo: {
    fontSize: 20
  },
  rowContainer: {
    flexDirection: 'row',
    padding: 10
  }
});

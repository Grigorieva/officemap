import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { NavBar } from 'react-native-router-flux';

import TT_COLORS from '../constants/colors';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    flexDirection: 'column',
    backgroundColor: TT_COLORS.TT_NAV_BAR_BLUE
  }
});

class NavigationBar extends Component {
  render() {
    return (
      <View style={styles.container}>
        <NavBar {...this.props} />
      </View>
    );
  }
}

export default NavigationBar;

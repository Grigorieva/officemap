
import Realm from 'realm';

class EmployeeLocation extends Realm.Object {
}
EmployeeLocation.schema = {
  name: 'EmployeeLocation',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    is_active: 'bool'
  }
};
class EmployeeDepartment extends Realm.Object {
}
EmployeeDepartment.schema = {
  name: 'EmployeeDepartment',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    alternative_name: 'string'
  }
};
class EmployeeTitle extends Realm.Object {
}
EmployeeTitle.schema = {
  name: 'EmployeeTitle',
  primaryKey: 'id',
  properties: {
    id: 'int',
    name: 'string',
    alternative_name: 'string'
  }
};


class Employee extends Realm.Object {
}

Employee.schema = {
  name: 'Employee',
  primaryKey: 'id',
  properties: {
    email: 'string',
    id: 'int',
    first_name: 'string',
    last_name: 'string',
    employee_location: 'EmployeeLocation',
    employee_department: 'EmployeeDepartment',
    employee_title: 'EmployeeTitle',
    skype: { type: 'string', optional: true },
    email_personal: { type: 'string', optional: true },
    phone_number: { type: 'string', optional: true },
    linkedin_url: { type: 'string', optional: true },
    cv_url: { type: 'string', optional: true },
    reports_to: { type: 'int', optional: true },
    show_skills: 'bool'
  }
};

class Image extends Realm.Object {
}
Image.schema = {
  name: 'Image',
  primaryKey: 'employeeId',
  properties: {
    employeeId: 'int',
    src: 'string'
  }
};

class Skill extends Realm.Object {}
Skill.schema = {
  name: 'Skill',
  primaryKey: 'id',
  properties: {
    id: 'int',
    measured: 'string',
    name: 'string',
    tree_path: 'string'
  }
};

class EmployeeSkill extends Realm.Object {
}
EmployeeSkill.schema = {
  name: 'EmployeeSkill',
  primaryKey: 'id',
  properties: {
    employee: 'int',
    id: 'int',
    level: 'int',
    attitude: 'int',
    note: { type: 'string', optional: true },
    date: { type: 'string', optional: true },
    skill: 'Skill'
  }
};

class EmployeeSkillShort extends Realm.Object {
}
EmployeeSkillShort.schema = {
  name: 'EmployeeSkillShort',
  primaryKey: 'uid',
  properties: {
    uid: { type: 'string', optional: false, indexed: true },
    employee_id: 'int',
    employee_name: 'string',
    skill_id: 'int',
    level: 'int',
    attitude: 'int',
    approved: { type: 'bool', optional: true }
  }
};

class SkillEmployee extends Realm.Object {
}
SkillEmployee.schema = {
  name: 'SkillEmployee',
  primaryKey: 'id',
  properties: {
    id: 'int', // skill id
    max_level: 'int',
    is_key_skill: 'bool',
    skill_name: 'string',
    catalog_name: 'string',
    owners: { type: 'list', objectType: 'EmployeeSkillShort' }
  }
};

export default new Realm({
  schema: [Image,
    Employee, EmployeeDepartment, EmployeeLocation, EmployeeTitle,
    Skill, EmployeeSkill, EmployeeSkillShort, SkillEmployee]
});


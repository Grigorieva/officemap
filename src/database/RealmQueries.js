
import RNFetchBlob from 'react-native-fetch-blob';
import myRealm from './MyRealm';

const EMPLOYEE = 'Employee';
const IMAGE = 'Image';
const EMPLOYEE_SKILL = 'EmployeeSkill';
const SKILL_EMPLOYEE = 'SkillEmployee';
const EMPLOYEE_SKILL_SHORT = 'EmployeeSkillShort';
const SKILL = 'Skill';

export function setEmployeesToDB(employees) {
  return new Promise((resolve, reject) => {
    myRealm.write(() => {
      for (const e of employees) {
        myRealm.create(EMPLOYEE, e, true);
      }
      resolve(employees);
    });
  });
}

export function getEmployeesFromDB() {
  return myRealm.objects(EMPLOYEE);
}

export function getEmployeeFromDB(id) {
  return myRealm.objects(EMPLOYEE)
        .filtered(`id = ${id}`)[0];
}

export function setSkillsToDB(skills) {
  myRealm.write(() => {
    for (const s of skills) {
      myRealm.create(SKILL, s, true);
    }
  });

  return Promise.resolve(skills);
}

export function setSkillEmployeesToDB(response) {
  return new Promise((resolve, reject) => {
    const parsedData = [];
    Object.keys(response).forEach((skillIdStr) => {
      const skillEmployees = response[skillIdStr];
      skillEmployees.id = parseInt(skillIdStr);

      const ownersDict = skillEmployees.owners;
      const ownerIds = Object.keys(ownersDict);
      skillEmployees.owners = ownerIds.map((ownerId) => {
        const employeeSkillShort = ownersDict[ownerId];
        employeeSkillShort.employee_id = employeeSkillShort.id;
        employeeSkillShort.employee_name = employeeSkillShort.name;
        employeeSkillShort.skill_id = parseInt(skillIdStr);
        employeeSkillShort.uid = `${employeeSkillShort.skill_id}-${employeeSkillShort.employee_id}`;
        return employeeSkillShort;
      });

      parsedData.push(skillEmployees);
    });
    myRealm.write(() => {
      parsedData.forEach((skillEmployees) => {
        myRealm.create(SKILL_EMPLOYEE, skillEmployees, true);
      });
      resolve(parsedData);
    });
  });
}

export function setEmployeeSkillsToDB(skills) {
  myRealm.write(() => {
    for (const s of skills) {
      myRealm.create(EMPLOYEE_SKILL, s, true);
    }
  });

  return Promise.resolve(skills);
}

export function getEmployeeSkillsFromDB(id) {
  const skills = myRealm.objects(EMPLOYEE_SKILL)
        .filtered(`employee = ${id}`);
  return Object.values(skills);
}

export function getEmployeesBySkillsFromDB() {
  return myRealm.objects(EMPLOYEE_SKILL);
}

export function getSkillEmployeesFromDB() {
  return myRealm.objects(SKILL_EMPLOYEE);
}

export function getSkillsFromDB() {
  return myRealm.objects(SKILL);
}

export function setImageToDB(id, src) {
  const obj = {
    employeeId: id,
    src: src
  };
  myRealm.write(() => {
    myRealm.create(IMAGE, obj, true);
  });
}

export function getImageFromDB(id) {
  return myRealm.objects(IMAGE)
        .filtered(`employeeId = ${id}`)[0].src;
}

export function clearData() {
  myRealm.write(() => {
    myRealm.delete(myRealm.objects(EMPLOYEE));
  });

  myRealm.write(() => {
    myRealm.delete(myRealm.objects(EMPLOYEE_SKILL));
  });

  myRealm.write(() => {
    myRealm.delete(myRealm.objects(SKILL_EMPLOYEE));
  });

  myRealm.write(() => {
    myRealm.delete(myRealm.objects(EMPLOYEE_SKILL_SHORT));
  });

  Object.values(myRealm.objects(IMAGE)).forEach((e) => {
    RNFetchBlob.fs.unlink(e.src);
  });

  myRealm.write(() => {
    myRealm.delete(myRealm.objects(IMAGE));
  });
}

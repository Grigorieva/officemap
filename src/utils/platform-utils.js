import { Platform, Dimensions } from 'react-native';

export function isiOS() {
  return (Platform.OS === 'ios');
}

export function notiOS() {
  return (Platform.OS !== 'ios');
}

export function isAndroid() {
  return (Platform.OS === 'android');
}

export function notAndroid() {
  return (Platform.OS !== 'android');
}


import {
    StyleSheet,
    View,
    AsyncStorage,
    ActivityIndicator,
    NetInfo
} from 'react-native';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import TT_COLORS from '../constants/colors';

export default class Startpage extends Component {
  static openEmployeesList() {
    Actions.drawer();
  }

  componentDidMount() {
    AsyncStorage.getItem('uid').then((id) => {
      if (id === null) {
        Actions.login();
      } else {
        NetInfo.isConnected.fetch().then((isConnected) => {
          if (isConnected) {
            fetch('https://planner.thumbtack.net/accounts/login/?next=/')
              .then((r) => {
                Startpage.openEmployeesList();
              });
          } else { Startpage.openEmployeesList(); }
        });
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size='large' />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

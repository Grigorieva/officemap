import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ActivityIndicator,
  Image,
  AsyncStorage,
  Dimensions,
  Keyboard,
  LayoutAnimation,
  Platform,
  UIManager
} from 'react-native';

import { Sae } from 'react-native-textinput-effects';

import { Actions } from 'react-native-router-flux';

import { loadAllData } from '../network/loadAllData';
import { login } from '../network/login';

import TT_COLORS from '../constants/colors';

const FontAwesome = require('react-native-vector-icons/FontAwesome');

export default class Login extends Component {
  state = {
    isLoading: false,
    loginString: '',
    passwordString: '',
    visibleHeight: Dimensions.get('window').height - 48
  };

  constructor(props) {
    super(props);
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }
  render() {
    return (
      <Image
        style={styles.container}
        source={require('./../../res/img/login_background.jpeg')}
        resizeMode={'cover'}
      >
        <View style={styles.loginFormContainer}>
          <View style={styles.loginFormHeader}>
            <Text style={styles.loginFormHeaderText}>{'TT Planner'}</Text>
          </View>
          <View style={styles.loginFormContentContainer}>
            {this.spinnet()}
            {this.inputField('LDAP Username', loginString => this.setState({ loginString }))}
            {this.inputField('LDAP Password', passwordString => this.setState({ passwordString }), true)}
            <TouchableHighlight
              style={styles.button}
              underlayColor='#99d9f4'
              onPress={this.onLoginPressed.bind(this)}
            >
              <Text style={styles.buttonText}>Login</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Image>
    );
  }

  spinnet = () => {
    return (this.state.isLoading
      ? <ActivityIndicator size='large' />
      : <View />);
  };

  inputField = (placeholder, func, secureTextEntry = false) => {
    return (
      <Sae
        style={styles.viewInput}
        labelStyle={styles.lableInput}
        inputStyle={styles.textInput}
        label={placeholder}
        iconClass={FontAwesome}
        iconName={'pencil'}
        iconColor={TT_COLORS.TT_LOGIN_TEXT_COLOR}
        // TextInput props
        autoCapitalize={'none'}
        autoCorrect={false}
        onChangeText={func}
        secureTextEntry={secureTextEntry}
      />
    );
  };

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    const newSize = Dimensions.get('window').height - e.endCoordinates.height - 48;
    this.setState({ visibleHeight: newSize });
  };

  keyboardDidHide = (e) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({ visibleHeight: Dimensions.get('window').height - 48 });
  };

  onLoginPressed() {
    if (!this.state.isLoading) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({ isLoading: true });

      const { loginString, passwordString } = this.state;
      this.props.perform(login)(loginString, passwordString)
      .then((responseForLogin) => {
        const uid = responseForLogin.url.split('employees/')[1].split('/skills/')[0];
        AsyncStorage.setItem('uid', uid);
        this.loadData();
        Login.openDrawer();
      })
      .catch(err => this.setState({ isLoading: false }));
    }
  }

  loadData = () => {
    AsyncStorage.getItem('isLoading').then((v) => {
      if (v === 'true') return;
      // loadAllData();
      AsyncStorage.setItem('isLoading', 'true');
    }); // на boolean ругается
  };

  static openDrawer() {
    Actions.drawer();
  }
}

const minDeviceDimension = () => {
  const size = Dimensions.get('window');
  return Math.min(size.width, size.height);
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: null,
    height: null
    // see: http://stackoverflow.com/questions/29322973/whats-the-best-way-to-add-a-full-screen-background-image-in-react-native
  },
  loginFormContainer: {
    borderBottomRightRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: TT_COLORS.TT_LIGHT_GRAY_BG_40_ALPHA
  },
  loginFormHeader: {
    width: minDeviceDimension() * 0.85 + 20,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: TT_COLORS.TT_BLUE,
  },
  loginFormHeaderText: {
    color: TT_COLORS.WHITE,
    fontSize: 20,
    alignSelf: 'center'
  },
  loginFormContentContainer: {
    margin: 10,
  },
  textInput: {
    color: TT_COLORS.TT_LOGIN_TEXT_COLOR,
    width: minDeviceDimension() * 0.85,
    fontSize: 20,
    fontWeight: 'bold'
  },
  viewInput: {
    width: minDeviceDimension() * 0.85,
    backgroundColor: TT_COLORS.TRANSPARENT
  },
  lableInput: {
    color: TT_COLORS.TT_LOGIN_TEXT_COLOR,
    fontSize: 16
  },
  buttonText: {
    fontSize: 20,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: TT_COLORS.TT_BLUE,
    borderColor: TT_COLORS.TT_BLUE,
    borderWidth: 1,
    borderRadius: 8,
    marginTop: 40,
    width: minDeviceDimension() * 0.85,
    justifyContent: 'center'
  }

});

import {
    ListView,
    RefreshControl,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Text,
    ActivityIndicator,
    LayoutAnimation,
    Platform,
    UIManager
} from 'react-native';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { getEmployees, getSkillEmployees } from '../../network/loadEmployees';
import { getSkills } from '../../network/loadSkills';
import EmployeeItem from '../../component/EmployeeItem';

import {
  filterEmployeesByName,
  filterEmployeesByIds,
  getSkilledEmployeeIds,
  skillIds
} from './logic';

import {
  getEmployeesFromDB,
  getSkillEmployeesFromDB,
  getSkillsFromDB,
  setEmployeesToDB,
  setSkillsToDB,
  setSkillEmployeesToDB
} from '../../database/RealmQueries';

import Filters from '../../component/Filters';
import InternetConnectionHandler from '../../network/InternetConnectionHandler';
import TT_COLORS from '../../constants/colors';
import { sceneTopMargin } from '../../constants/platform';

export default class EmployeesList extends Component {
  constructor(props) {
    super(props);

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.netHandler = new InternetConnectionHandler();
  }

  componentWillMount() {
    const params = {};
    params.renderRightButton = this.renderShowMoreButton;
    Actions.refresh(params);
  }

  componentDidMount() {
    const skillEmployees = this.querySkillEmployees();
    const employees = this.queryEmployees();
    const skills = this.querySkills();

    this.mountSkillEmployees(skillEmployees);
    this.mountEmployees(employees);
    this.mountSkills(skills);

    this.setupInitialState(employees, skillEmployees, skills);

    this.netHandler.startTracking((isConnected) => {
      if (isConnected) {
        this.onRefresh();
        this.updateSkillsFromNetwork();
      }
    });
  }

  componentWillUnmount() {
    this.netHandler.stopTracking();

    this.unmountEmployees(this.state.employees);
    this.unmountSkillEmployees(this.state.skillEmployees);
    this.unmountSkills(this.state.skills);
  }

  state = {
    isPullToRefreshActive: false,
    isLoading: true,
    dataSource: new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    }),
    employees: null,
    skillEmployees: null,
    skills: '',
    employeeSkills: null,
    searchText: '',
    selectedSkills: [],
    lastSelectedSkills: '',
    showMore: false
  };

  renderShowMoreButton = () => {
    return (
      <TouchableOpacity
        style={{ paddingHorizontal: 15, paddingBottom: 30 }}
        onPress={() => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
          this.setState({ showMore: !this.state.showMore });
        }}
      >
        <Image
          source={require('../../../res/img/show_more.png')}
          style={{ tintColor: TT_COLORS.WHITE }}
        />
      </TouchableOpacity>
    );
  };

  /**
   * Function that retrieves employees from server
   * and inserts new records in Realm or updates existing
   */
  updateEmployeesFromNetwork = () => {
    return this.props.perform(getEmployees)()
      .then((response) => {
        return setEmployeesToDB(response);
      });
  };

  updateSkillEmployeesFromNetwork = (selectedSkills) => {
    return this.props.perform(getSkillEmployees)(skillIds(selectedSkills))
      .then((data) => {
        return setSkillEmployeesToDB(data);
      });
  };

  onLoadingFinished = () => {
    this.setState({ isLoading: false, isPullToRefreshActive: false });
  };

  onLoadingStart = (pullToRefresh = false) => {
    this.setState({ isLoading: true, isPullToRefreshActive: pullToRefresh });
  };

  onRefresh = (pullToRefresh = false) => {
    this.onLoadingStart(pullToRefresh);
    const { selectedSkills } = this.state;
    Promise.all([this.updateEmployeesFromNetwork(), this.updateSkillEmployeesFromNetwork(selectedSkills)])
      .then(this.onLoadingFinished)
      .catch(this.onLoadingFinished);
  };

  onSelectedSkillsChanged = (selectedSkills) => {
    this.onLoadingStart(false);
    this.updateSkillEmployeesFromNetwork(selectedSkills)
      .then(this.onLoadingFinished)
      .catch(this.onLoadingFinished);
  };

  /**
   * Function that retrieves employees from Realm
   * @returns {Realm.Results} https://realm.io/docs/javascript/latest/#queries
   */
  queryEmployees = () => {
    return getEmployeesFromDB();
  };

  setupInitialState = (employees, skillEmployees, skills) => {
    const { searchText, selectedSkills } = this.state;
    this.setState({
      employees: employees,
      skillEmployees: skillEmployees,
      skills: skills,
      dataSource: this.updateDataSource(searchText, employees, skillEmployees, selectedSkills)
    });
  };

  mountEmployees = (employees) => {
    employees.addListener((theEmployees, changes) => {
      const { searchText, skillEmployees, selectedSkills } = this.state;
      this.setState({
        dataSource: this.updateDataSource(searchText, theEmployees, skillEmployees, selectedSkills)
      });
    });
  };

  filterEmployees = (searchText, employees, skillEmployees, selectedSkills) => {
    let filteredEmployees = Object.values(employees);
    filteredEmployees = filterEmployeesByName(filteredEmployees, searchText);

    if (selectedSkills.length !== 0) {
      const desiredEmployeeIds = getSkilledEmployeeIds(skillEmployees, selectedSkills);
      filteredEmployees = filterEmployeesByIds(filteredEmployees, desiredEmployeeIds);
    }

    return filteredEmployees;
  };

  querySkillEmployees = () => {
    return getSkillEmployeesFromDB();
  };

  mountSkillEmployees = (skillEmployees) => {
    skillEmployees.addListener((theSkillEmployees, changes) => {
      const { searchText, employees, selectedSkills } = this.state;
      this.setState({
        skillEmployees: theSkillEmployees,
        dataSource: this.updateDataSource(searchText, employees, skillEmployees, selectedSkills)
      });
    });
  };

  unmountSkillEmployees = (skillEmployees) => {
    if (skillEmployees !== null) {
      skillEmployees.removeAllListeners();
    }
  };

  unmountEmployees = (employees) => {
    if (employees !== '' && employees !== null) {
      employees.removeAllListeners();
    }
  };

  /**
   * Function that retrieves employees from server
   * and inserts new records in Realm or updates existing
   */
  updateSkillsFromNetwork = () => {
    this.props.perform(getSkills)()
      .then(setSkillsToDB);
  };

  /**
   * Function that retrieves skills from Realm
   * @returns {Realm.Results} https://realm.io/docs/javascript/latest/#queries
   */
  querySkills = () => {
    return getSkillsFromDB();
  };

  mountSkills = (skills) => {
    skills.addListener((theSkills, changes) => {
      this.setState({ skills: theSkills });
    });
  };

  unmountSkills = (skills) => {
    if (skills !== null) {
      skills.removeAllListeners();
    }
  };

  updateDataSource = (searchText, employees, skillEmployees, selectedSkills) => {
    return this.state.dataSource.cloneWithRows(this.filterEmployees(searchText, employees, skillEmployees, selectedSkills));
  };

  renderRow = (rowData, sectionID, rowID) => {
    return (
      <EmployeeItem
        employee={rowData}
        onPress={data => this.rowPressed(data)}
      />
    );
  };

  rowPressed = (rowData) => {
    Actions.employeeInfo({
      id: rowData.id,
      title: `${rowData.first_name} ${rowData.last_name}`
    });
  };

  content = () => {
    const { isLoading, isPullToRefreshActive } = this.state;
    if (isLoading && this.state.dataSource.getRowCount() === 0 && !isPullToRefreshActive) {
      return (
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 20 }}>
          <ActivityIndicator size='large' />
        </View>
      );
    }
    return (
      <View style={styles.employeesContainer}>
        <ListView
          refreshControl={
            <RefreshControl
              refreshing={this.state.isPullToRefreshActive}
              onRefresh={() => {
                this.onRefresh(true);
              }}
            />
            }
          renderHeader={() => { return this.renderEmptyListHeader(); }}
          removeClippedSubviews={true}
          dataSource={this.state.dataSource}
          enableEmptySections={true}
          renderRow={this.renderRow.bind(this)}
          renderSeparator={
                            (sectionId, rowId) => <View key={rowId} style={styles.separator} />
                          }
        />
      </View>
    );
  };

  renderEmptyListHeader = () => {
    if (this.state.dataSource.getRowCount() === 0) {
      return (
        <View style={styles.noEmployeesHeaderContainer}>
          <Text style={{ fontSize: 18 }}>No Employees</Text>
        </View>
      );
    }

    return null;
  };

  filters = () => {
    return (
      <Filters
        onChangeText={(text) => {
          const { employees, skillEmployees, selectedSkills } = this.state;
          this.setState({
            searchText: text,
            dataSource: this.updateDataSource(text, employees, skillEmployees, selectedSkills)
          });
        }}
        onSkillSelected={(skills) => {
          this.onSelectedSkillsChanged(skills);
          const { searchText, employees, skillEmployees } = this.state;
          this.setState({
            selectedSkills: skills,
            dataSource: this.updateDataSource(searchText, employees, skillEmployees, skills)
          });
        }}
        skills={Object.values(this.state.skills)}
        allFilters={this.state.showMore}
      />
    );
  };

  render() {
    return (
      <View style={styles.contentContainer}>
        {this.filters()}
        {this.content()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    flex: 1,
    backgroundColor: TT_COLORS.TT_LIGHT_GRAY_BG,
    marginTop: sceneTopMargin
  },
  employeesContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: TT_COLORS.TT_LIGHT_GRAY_BG
  },
  noEmployeesHeaderContainer: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: TT_COLORS.TT_LIGHT_GRAY_BG
  },
  separator: {
    height: 1,
    backgroundColor: TT_COLORS.TT_BORDER_GRAY
  }
});

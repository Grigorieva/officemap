export function filterEmployeesByName(employees, searchText) {
  const text = (typeof searchText === 'undefined' || searchText === null) ? '' : searchText;
  const filteredEmployees = Object.values(employees);
  if (text === '') { return filteredEmployees; }
  const theFilter = (employee) => {
    const fullName = `${employee.first_name} ${employee.last_name}`;
    return fullName.toLowerCase().search(text.toLowerCase()) >= 0;
  };
  return filteredEmployees.filter(theFilter);
}

export function getSkilledEmployeeIds(skillEmployees, selectedSkills) {
  if (skillEmployees === null) {
    return [];
  }

  const theSkillIds = skillIds(selectedSkills);

  let filteredSkillEmployees = Object.values(skillEmployees);
  filteredSkillEmployees = filteredSkillEmployees.filter((theSkillEmployees) => {
    return theSkillIds.indexOf(theSkillEmployees.id) >= 0;
  });

  const ownerIdMatrix = filteredSkillEmployees
    .map((theSkillEmployees) => {
      return Object.values(theSkillEmployees.owners)
        .map(owner => owner.employee_id);
    })
    .sort((ownerIds1, ownerIds2) => {
      return ownerIds1.length - ownerIds2.length;
    });

  if (ownerIdMatrix.length === 0) {
    return [];
  }

  return ownerIdMatrix.shift()
    .filter((ownerId) => {
      return ownerIdMatrix.every((ownerIds) => {
        return ownerIds.indexOf(ownerId) !== -1;
      });
    });
}

export function filterEmployeesByIds(employees, ids) {
  let filteredEmployees = employees;
  if (ids.length === 0) {
    return [];
  }

  try {
    const filter = row => ids.search(`"${row.id}"`) >= 0;
    filteredEmployees = filteredEmployees.filter(filter);
  } catch (e) {
    const filter = row => ids.indexOf(row.id) >= 0;
    filteredEmployees = filteredEmployees.filter(filter);
  }

  return filteredEmployees;
}

export function skillIds(skills) {
  if (typeof skills === 'undefined' || skills === null || skills.length === 0) {
    return [];
  }

  return skills.map(skill => skill.id);
}

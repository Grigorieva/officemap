import React, { Component, PropTypes } from 'react';
import {
    Image,
    StyleSheet,
    View,
    Text,
    TouchableHighlight,
    ScrollView,
    ActivityIndicator,
    LayoutAnimation,
    Platform,
    UIManager,
    Dimensions,
    Linking,
    AsyncStorage,
    NetInfo,
    InteractionManager
} from 'react-native';
import { ListView } from 'realm/react-native';
import Communications from 'react-native-communications';
import { getEmployeeInfo } from '../network/loadEmployeeInfo';
import { getEmployeeSkills } from '../network/loadEmployeeSkills';
import SkillItem from '../component/SkillItem';
import { getEmployeeFromDB, getImageFromDB, getEmployeeSkillsFromDB } from '../database/RealmQueries';

import TT_COLORS from '../constants/colors';
import { sceneTopMargin } from '../constants/platform';

const imagePlaceholder = require('./../../res/img/ic_account_circle_white_48dp.png');
const md5 = require('./../utils/js-md5');

const minScreenSize = Math.min(Dimensions.get('window').width, Dimensions.get('window').height);
const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2
});
const skillCellsInRow = parseInt(Dimensions.get('window').width / 130 * 2, 10);

export default class EmployeeInfo extends Component {
  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      if (Platform.OS === 'android') {
        UIManager.setLayoutAnimationEnabledExperimental(true);
      }
      if (this.props.myProfile) {
        AsyncStorage.getItem('uid', (err, id) => {
          this.loadData(id);
        });
      } else {
        this.loadData(this.props.id);
      }
      this.setState({ interactionsComplete: true });
    });
  }

  loadData = (id) => {
    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected) {
        this.loadNetwork(id);
      } else {
        this.getLocalData(id);
        this.setState({ isFetching: false });
      }
    });
  };

  state = {
    interactionsComplete: false,
    email: '',
    id: '',
    first_name: '',
    last_name: '',
    employee_location: {
      id: '',
      name: '',
      is_active: ''
    },
    employee_department: {
      id: '',
      name: '',
      alternative_name: ''
    },
    employee_title: {
      id: '',
      name: '',
      alternative_name: ''
    },
    skype: '',
    email_personal: '',
    phone_number: '',
    linkedin_url: '',
    cv_url: '',
    reports_to: '',
    show_skills: false,
    imgSizeMini: true,
    skills: '',
    fullSkills: false,
    scrollOffset: 0
  };

  loadNetwork = (id) => {
    this.props.perform(getEmployeeInfo)(id).then((data) => {
      this.setUserInfo(data);
      this.setState({ thumbUri: this.getURI(data.email) });

      AsyncStorage.getItem('uid').then((uid) => {
        if (uid === data.id || this.state.show_skills) {
          this.getEmployeeSkills(data.id);
        }
      });
    });
  };

  getLocalData = (id) => {
    const data = getEmployeeFromDB(id);
    this.setUserInfo(data);
    const uri = getImageFromDB(data.id);
    this.setState({ thumbUri: uri });
    const skills = getEmployeeSkillsFromDB(id);
        // console.log(skills);
    this.setSkills(skills);
  };

  setUserInfo = (data) => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState(data);
  };

  userInfo = () => {
    const containerStyle = this.state.imgSizeMini ? styles.container : styles.containerCol;
    if (this.state.id === '') {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator size='large' />
        </View>
      );
    }
    return (
      <ScrollView
        style={styles.containerCol}
        onScroll={this.handleScroll}
      >
        <View style={containerStyle}>
          {this.thumb()}
          {this.infoContent()}
        </View>
        {this.userSkills()}
      </ScrollView>
    );
  };

  handleScroll = (event) => {
    const overheight = event.nativeEvent.contentSize.height - event.nativeEvent.layoutMeasurement.height;
    const offset = event.nativeEvent.contentOffset.y;
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({ scrollOffset: offset - overheight });
  };

  button = (prop, func, text) => {
    const buttonText = text === null ? prop : text;
    if (prop !== '') {
      return (
        <TouchableHighlight
          onPress={() => func(prop)}
          underlayColor='transparent'
        >
          <Text style={styles.clickableInfo}>{buttonText}</Text>
        </TouchableHighlight>);
    }
    return <View />;
  };

  skype = () => {
    return this.state.skype !== '' ? <Text style={styles.info}>Skype: {this.state.skype}</Text> : <View />;
  };

  getURI = (email) => {
    return `https://secure.gravatar.com/avatar/${md5(email)}.jpg?s=400&amp;r=g&amp;d=mm`;
  };

  thumb = () => {
    return (
      <TouchableHighlight
        underlayColor='transparent' onPress={() => {
          LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
          this.setState({
            imgSizeMini: !this.state.imgSizeMini
          });
        }}
      >
        <Image
          style={this.state.imgSizeMini ? styles.thumbMini : styles.thumb}
          source={{ uri: this.state.thumbUri }}
          defaultSource={imagePlaceholder}
        />
      </TouchableHighlight>);
  };

  infoContent = () => {
    return (
      <View style={styles.infoContainer}>
        <Text style={styles.info}>{this.state.employee_title.name}</Text>
        <Text style={styles.info}>Department: {this.state.employee_department.name}</Text>
        <View style={styles.separator} />
        {this.button(this.state.phone_number, this.phonePressed)}
        {this.skype()}
        <Text style={styles.info}>{this.state.employee_location.name}</Text>
        {this.button(this.state.email, this.emailPressed)}
        {this.button(this.state.email_personal, this.emailPressed)}
        {this.button(this.state.linkedin_url, this.webPressed, 'linkedIn')}
        {this.button(this.state.cv_url, this.webPressed, 'cv')}
      </View>
    );
  };

  userSkills = () => {
    return this.state.skills === '' ? <View /> : (
      <View>
        <ListView
          contentContainerStyle={styles.list} dataSource={this.state.dataSource}
          enableEmptySections={true}
          renderRow={this.renderRow}
        />
        {this.moreSkillsButton()}
      </View>
            );
  };

  moreSkillsButton = () => {
    if (this.state.skills.length < skillCellsInRow) {
      return <View />;
    }
    let style, text, fontSize;
    if (this.state.fullSkills === false) {
      style = styles.showMoreSkillsButton;
      text = 'Show more';
      fontSize = 22;
    } else {
      style = [styles.hideMoreSkillsButton,
                { bottom: 20 - this.state.scrollOffset }];
      text = '—';
      fontSize = 30;
    }
    return (
      <TouchableHighlight
        onPress={() => this.onSkillsButtonPress()}
        style={style}
      >
        <Text
          style={{
            fontSize: fontSize,
            color: TT_COLORS.WHITE,
            fontWeight: 'bold'
          }}
        >{text}</Text>
      </TouchableHighlight>);
  };

  renderRow = (rowData, sectionID, rowID) => {
    return <SkillItem skill={rowData} />;
  };

  render() {
    if (!this.state.interactionsComplete) {
      return <View />;
    }

    return this.userInfo();
  }

  getEmployeeSkills(id) {
    this.props.perform(getEmployeeSkills)(id).then(this.setSkills);
  }

  setSkills = (data) => {
    if (data.length > 1) {
      data.sort((a, b) => {
        if (a.level === b.level) { return b.attitude - a.attitude; }
        return b.level - a.level;
      });
    }
    this.setState({
      skills: data,
      dataSource: ds.cloneWithRows(data.slice(0, skillCellsInRow))
    });
  };

  phonePressed = (data) => {
    Communications.phonecall(data, true);
  };

  emailPressed = (data) => {
    Linking.openURL(`mailto:${data}`);
  };

  webPressed = (data) => {
    Communications.web(data);
  };

  onSkillsButtonPress() {
    let skills;
    if (this.state.fullSkills) { skills = this.state.skills.slice(0, skillCellsInRow); } else { skills = this.state.skills; }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({
      fullSkills: !this.state.fullSkills,
      dataSource: ds.cloneWithRows(skills),
      scrollOffset: 0
    });
  }
}

EmployeeInfo.propTypes = {
  id: PropTypes.number
};

const styles = StyleSheet.create({
  thumbMini: {
    width: minScreenSize * 0.3,
    height: minScreenSize * 0.3,
    borderRadius: minScreenSize / 10,
    borderBottomRightRadius: 0
  },
  thumb: {
    width: minScreenSize,
    height: minScreenSize,
    borderRadius: minScreenSize / 4
  },
  infoContainer: {
    flex: 1,
    padding: 10
  },
  info: {
    fontSize: 18,
    color: TT_COLORS.TT_BLUE_BLACK_TEXT
  },
  clickableInfo: {
    fontSize: 20,
    color: TT_COLORS.TT_BLUE
  },
  container: {
    flexDirection: 'row',
    padding: 3
  },
  containerCol: {
    flexDirection: 'column',
    marginTop: sceneTopMargin
  },
  separator: {
    height: 1,
    margin: 3,
    backgroundColor: TT_COLORS.TT_BORDER_GRAY
  },
  list: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  showMoreSkillsButton: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: Dimensions.get('window').width * 0.8,
    height: 35,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: TT_COLORS.TT_BORDER_GRAY,
    backgroundColor: TT_COLORS.TT_BLUE
  },
  hideMoreSkillsButton: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'flex-end',
    right: 20,
    bottom: 20,
    width: 40,
    height: 40,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: TT_COLORS.TT_BORDER_GRAY,
    backgroundColor: TT_COLORS.TT_BLUE
  }
});

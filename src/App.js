import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  StatusBar,
  Image,
  TouchableOpacity
} from 'react-native';

import { Actions, Scene, Router, ActionConst } from 'react-native-router-flux';

import Startpage from './screens/Startpage';
import Login from './screens/Login';
import EmployeeInfo from './screens/EmployeeInfo';
import EmployeesList from './screens/employees/EmployeesList';

import NavigationDrawer from './component/NavigationDrawer';
import NavigationBar from './component/NavigationBar';

import * as platformUtils from './utils/platform-utils';
import platformConst from './constants/platform';

import TT_COLORS from './constants/colors';
import { perform, production } from './network/RestClient';

const renderDrawerButton = () => {
  return (
    <TouchableOpacity
      onPress={() => {
        Actions.refresh({ key: 'drawer', open: true });
      }}
      style={{ paddingLeft: 16, paddingRight: 80, paddingBottom: 30 }}
    >
      <Image
        source={require('../res/img/navicon_menu.png')}
        style={{ tintColor: TT_COLORS.WHITE }}
      />
    </TouchableOpacity>
  );
};

const getSceneStyle = (props, computedProps) => {
  const style = {
    flex: 1,
    backgroundColor: TT_COLORS.WHITE,
    shadowColor: null,
    shadowOffset: null,
    shadowOpacity: null,
    shadowRadius: null
  };

  if (computedProps.isActive) {
    if (platformUtils.isAndroid) {
      style.marginTop = computedProps.hideNavBar ? 0 : 44;
    }
  }

  return style;
};

class App extends Component {
  renderScenes = () => {
    const navBarProps = {
      navBar: NavigationBar,
      navigationBarStyle: styles.navBarStyle,
      barButtonIconStyle: styles.barButtonIconStyle,
      backButtonBarStyle: styles.barButtonIconStyle,
      titleStyle: styles.titleStyle,
    };

    const apiProps = {
      config: production,
      perform: perform.bind(production)
    };

    if (platformUtils.isAndroid()) {
      StatusBar.setHidden(true);
    } else if (platformUtils.isiOS()) {
      StatusBar.setBarStyle('light-content');
    }

    return (
      Actions.create(
        <Scene key='root'>
          <Scene
            key='start'
            initial
            component={Startpage}
            type={ActionConst.REPLACE}
            {...apiProps}
            hideNavBar
            hideTabBar
          />
          <Scene
            key='login'
            component={Login}
            type={ActionConst.REPLACE}
            {...navBarProps}
            {...apiProps}
            hideTabBar
            hideNavBar
          />
          <Scene
            key='drawer'
            component={NavigationDrawer}
            type={ActionConst.REPLACE}
            {...navBarProps}
            {...apiProps}
            open={false}
          >
            <Scene
              key='main'
              tabs={true}
            >
              <Scene
                key='employeesList'
                component={EmployeesList}
                title={'Employees List'}
                {...navBarProps}
                {...apiProps}
                hideTabBar
                renderLeftButton={renderDrawerButton}
              />
              <Scene
                key='myProfile'
                component={EmployeeInfo}
                {...navBarProps}
                {...apiProps}
                hideTabBar
                myProfile={true}
                renderLeftButton={renderDrawerButton}
              />
            </Scene>
          </Scene>
          <Scene
            key='employeeInfo'
            type={ActionConst.PUSH}
            component={EmployeeInfo}
            {...navBarProps}
            {...apiProps}
            hideTabBar
          />
        </Scene>
      )
    );
  };

  render() {
    return (
      <Router
        scenes={this.renderScenes()}
        getSceneStyle={getSceneStyle}
        backButtonBarStyle={styles.barButtonIconStyle}
        barButtonIconStyle={styles.barButtonIconStyle}
      />
    );
  }
}

const styles = StyleSheet.create({
  navBarStyle: {
    position: 'relative',
    paddingTop: 0,
    borderBottomWidth: 0,
    top: null,
    ...Platform.select({
      ios: {
        height: platformConst.iosNavBarHeight
      },
      android: {
        height: platformConst.androidNavBarHeight
      }
    }),
    right: 0,
    left: 0,
    backgroundColor: TT_COLORS.TT_NAV_BAR_BLUE
  },
  titleStyle: {
    textAlign: 'center',
    color: TT_COLORS.WHITE,
    fontSize: 17,
    fontWeight: '600',
    fontFamily: 'System'
  },
  barButtonIconStyle: {
    tintColor: TT_COLORS.WHITE
  }
});

export default App;


import { request } from './RestClient';

export function getEmployees() {
  return request.bind(this)('api-v1/employees/?format=json', {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(response => response.json());
}

export function getSkillEmployees(ids) {
  let url = 'employees/ajax_get_skills_employees/?';
  ids.forEach((e) => {
    url += `skill_ids[]=${e}&`;
  });
  url = url.replace(new RegExp('&$'), '');

  return request.bind(this)(url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(response => response.json());
}

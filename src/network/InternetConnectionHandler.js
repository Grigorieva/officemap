import {
  NetInfo
} from 'react-native';

export default class InternetConnectionHandler {
  connectivityCallback = null;
  isConnected = null;

  startTracking = (callback) => {
    NetInfo.isConnected.addEventListener('change', callback);
    NetInfo.isConnected.fetch().done((isConnected) => { this.isConnected = isConnected; callback(isConnected); });
  };

  stopTracking = () => {
    if (this.connectivityCallback !== null) {
      NetInfo.isConnected.removeEventListener('change', this.connectivityCallback);
    }
  };
}

import { clearData, setEmployeesToDB, setEmployeeSkillsToDB, setSkillsToDB } from './../database/RealmQueries';
import { getEmployees } from './loadEmployees';
import { getSkills } from './loadSkills';
import { getImages } from './loadImages';
import { getEmployeeSkills } from './loadEmployeeSkills';

export function loadAllData() {
  clearData();
    // return Promise.all(
    // getEmployees()
    //     .then(employees => {
    //         Promise.all(
    //             setEmployeesToDB(employees),
    //             getImages(employees),
    //         )
    //     }),
    // getSkills()
    //     .then(setSkillsToDB)
    // )
    // return getEmployees()
    //     .then(employees => {
    //         Promise.all(
    //             setEmployeesToDB(employees),
    //             getImages(employees),
    //             employees.forEach(e => {
    //                 getEmployeeSkills(e.id)
    //                     .then(skills => {
    //                         console.log(e.id);
    //                         if (skills.detail === null)
    //                             setEmployeeSkillsToDB(skills)
    //                     })
    //             })
    //         )
    //             .then(console.log('a'));
    //     })
    //     .then(() => {
    //         console.log(b);
    //         Promise.resolve()
    //     });
  return getEmployees()
        .then(setEmployeesToDB)
        .then(getImages)
        .then((employees) => {
          const actions = employees.map(fn);
          Promise.all(actions);
        })
        .then(getSkills)
        .then(setSkillsToDB);


    // .then(employees => {
    //     employees.forEach(e => {
    //         getEmployeeSkills(e.id)
    //             .then(skills => setEmployeeSkillsToDB(skills, e))
    //     })
    // })
}

let fn = function getSkills(employee) {
  getEmployeeSkills(employee.id)
        .then((skills) => {
          if (skills.detail === undefined) {
            setEmployeeSkillsToDB(skills);
          } else return Promise.resolve();
        });
};

import { request } from './RestClient';

export function getEmployeeSkills(id) {
  return request.bind(this)(`api-v1/employees/${id}/skills/?format=json`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(response => response.json());
}

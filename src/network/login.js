import { request } from './RestClient';

export function login(username, password) {
  const config = this;
  return request.bind(config)('accounts/login/?next=/').then((responseForCsrf) => {
    const cook = responseForCsrf.headers.get('set-cookie');
    const csrfmiddlewaretoken = cook.split('csrftoken=')[1].split(';')[0];
    let data;
    if (username === '' || password === '') {
      data = `username=egrigorieva&password=${encodeURIComponent('TFGaFCbEYWUKV3cgnJfY!')}&csrfmiddlewaretoken=${csrfmiddlewaretoken}`;
    } else {
      data = `username=${username}&password=${encodeURIComponent(password)}&csrfmiddlewaretoken=${csrfmiddlewaretoken}`;
    }
    return request.bind(config)('accounts/login/', {
      method: 'POST',
      headers: {
        Referer: `${config.baseUrl}accounts/login/`,
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: data
    });
  });
}

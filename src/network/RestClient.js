export const production = {
  baseUrl: 'https://planner.thumbtack.net/'
};

export const demo = {
  baseUrl: 'https://rt-demo.dev.thumbtack.net/'
};

// to set config: perform = perform.bind(config);
// and then: perform(request)(...args)
// or request.bind(config)(...args)
export function perform(theRequest) {
  return (...args) => { return theRequest.bind(this)(...args); };
}

export function request(path, params) {
  const config = this;
  const url = `${config.baseUrl}${path}`;
  console.log(`RestClient: ${url}`);
  return fetch(url, params);
}

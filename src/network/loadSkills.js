import { request } from './RestClient';

export function getSkills() {
  return request.bind(this)('api-v1/skills/?format=json', {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(response => response.json());
}

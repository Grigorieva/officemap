import RNFetchBlob from 'react-native-fetch-blob';
const md5 = require('./../utils/js-md5');
import { setImageToDB } from './../database/RealmQueries';
import { Platform } from 'react-native';

export function getImages(employees) {
  employees.forEach((e, i) => {
    RNFetchBlob
      .config({ fileCache: true })
      .fetch('GET', getURI(e.email))
      .then((res) => {
        const path = Platform.OS === 'android' ? `file://${res.path()}` : `${res.path()}`;
        setImageToDB(e.id, path);
      });
  });
  return Promise.resolve(employees);
}


getURI = (email) => {
  return `https://secure.gravatar.com/avatar/${md5(email)}.jpg?s=400&amp;r=g&amp;d=mm`;
};

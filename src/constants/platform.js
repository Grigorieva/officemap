import { isiOS } from '../utils/platform-utils';

module.exports = Object.freeze({
  iosNavBarHeight: 64,
  androidNavBarHeight: 56,
  sceneTopMargin: isiOS() ? 20 : 12
});

package com.officemap;

import android.support.annotation.Nullable;

import android.app.Application;
import com.facebook.react.ReactApplication;

import com.RNFetchBlob.RNFetchBlobPackage;
import com.crashlytics.android.Crashlytics;

import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.facebook.react.ReactPackage;
import com.facebook.react.modules.network.OkHttpClientProvider;
import com.facebook.react.modules.network.ReactCookieJarContainer;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import io.realm.react.RealmReactPackage;

import com.smixx.fabric.FabricPackage;


import okhttp3.OkHttpClient;

//public class MainApplication extends NavigationApplication {
//
//    @Override
//    public boolean isDebug() {
//        // Make sure you are using BuildConfig from your own application
//        return BuildConfig.DEBUG;
//    }
//
//    @Nullable
//    @Override
//    public List<ReactPackage> createAdditionalReactPackages() {
//        return Arrays.asList(
//                new RealmReactPackage(),
//                new RNFetchBlobPackage(),
//                new FabricPackage());
//        // Add the packages you require here.
//        // No need to add RnnPackage and MainReactPackage
//    }
//
//    public void onCreate() {
//        super.onCreate();
//        Fabric.with(this, new Crashlytics());
//        Stetho.initializeWithDefaults(this);
//        OkHttpClient client = new OkHttpClient.Builder()
//                .connectTimeout(0, TimeUnit.MILLISECONDS)
//                .readTimeout(0, TimeUnit.MILLISECONDS)
//                .writeTimeout(0, TimeUnit.MILLISECONDS)
//                .cookieJar(new ReactCookieJarContainer())
//                .addNetworkInterceptor(new StethoInterceptor())
//                .build();
//        OkHttpClientProvider.replaceOkHttpClient(client);
//    }
//
//}

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    protected boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
              new MainReactPackage(),
              new RealmReactPackage(),
              new RNFetchBlobPackage(),
              new FabricPackage()
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Stetho.initializeWithDefaults(this);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(0, TimeUnit.MILLISECONDS)
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .writeTimeout(0, TimeUnit.MILLISECONDS)
                .cookieJar(new ReactCookieJarContainer())
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        OkHttpClientProvider.replaceOkHttpClient(client);
    }
}